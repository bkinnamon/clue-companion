const eliminated = false

const suspectNames = [
  'Green',
  'Mustard',
  'Peacock',
  'Plum',
  'Scarlett',
  'White',
]

export const suspectList = suspectNames.map((name) => ({
  name,
  eliminated,
}))

const weaponNames = [
  'Candlestick',
  'Dagger',
  'Lead Pipe',
  'Pistol',
  'Rope',
  'Wrench',
]

export const weaponList = weaponNames.map((name) => ({
  name,
  eliminated,
}))

const mansionPlaceNames = [
  'Bathroom',
  'Bedroom',
  'Courtyard',
  'Dining Room',
  'Game Room',
  'Garage',
  'Kitchen',
  'Living Room',
  'Office',
]

export const mansionPlaceList = mansionPlaceNames.map((name) => ({
  name,
  eliminated,
}))

const boardwalkPlaceNames = [
  'Arcade',
  'Beach',
  'Beach House',
  'Ferris Wheel',
  'Jet Ski Rental',
  'Surf Shop',
]

export const boardwalkPlaceList = boardwalkPlaceNames.map((name) => ({
  name,
  eliminated,
}))

export const boards = {
  boardwalk: {
    name: 'Boardwalk',
    placeList: boardwalkPlaceList,
  },
  mansion: {
    name: 'Mansion',
    placeList: mansionPlaceList,
  },
}
