import React, { useState, Suspense } from 'react'
import _ from 'lodash'
import { suspectList, boards, weaponList } from './clues'
import { getFromStorage, saveToStorage } from './services/storage'

import { Layout } from './components/Layout'
import { PickerDialog } from './components/PickerDialog'
import { OptionList } from './components/OptionList'

const initialColor =
  getFromStorage('color') ||
  suspectList[Math.floor(Math.random() * suspectList.length)].name.toLowerCase()
saveToStorage('color', initialColor)

const boardOptions = Object.entries(boards).map(([key, board]) => ({
  text: board.name,
  value: key,
}))

const characterOptions = suspectList.map((suspect) => ({
  text: suspect.name,
  value: suspect.name.toLowerCase(),
}))

export const App = () => {
  const [board, setBoard] = useState(boards.mansion)
  const [color, setColor] = useState(initialColor)
  const [picker, setPicker] = useState(null)
  const [suspects, setSuspects] = useState(suspectList)
  const [weapons, setWeapons] = useState(weaponList)
  const [places, setPlaces] = useState(board.placeList)

  const actions = [
    {
      text: 'Change Character',
      do: () => {
        setPicker({
          title: 'Choose a Character',
          options: characterOptions,
          onPick(option) {
            if (option !== null) {
              saveToStorage('color', option.value)
              setColor(option.value)
            }
            setPicker(null)
          },
        })
      },
    },
    {
      text: 'Change Board',
      do: () => {
        setPicker({
          title: 'Choose a Board',
          options: boardOptions,
          onPick(option) {
            if (option) {
              setBoard(boards[option.value])
              setPlaces(boards[option.value].placeList)
              resetSheet()
            }
            setPicker(null)
          },
        })
      },
    },
    {
      text: 'Reset Sheet',
      do: () => {
        resetSheet()
      },
    },
  ]

  function resetSheet() {
    setPicker({
      title: 'This will reset your sheet. Are you sure?',
      options: [
        { text: 'Yes', value: true },
        { text: 'No', value: false },
      ],
      onPick(option) {
        if (option && option.value) {
          setSuspects(suspects.map((s) => ({ ...s, eliminated: false })))
          setWeapons(weapons.map((s) => ({ ...s, eliminated: false })))
          setPlaces(places.map((s) => ({ ...s, eliminated: false })))
        }
        setPicker(null)
      },
    })
  }

  function toggle(list, item, set) {
    set(
      list.map((i) => {
        if (i.name !== item.name) return i
        return {
          ...i,
          eliminated: !i.eliminated,
        }
      })
    )
  }

  return (
    <div className={color}>
      <Layout actions={actions} title={board.name}>
        <OptionList
          options={suspects}
          title="Suspects"
          onChoose={(suspect) => toggle(suspects, suspect, setSuspects)}
        />
        <OptionList
          options={weapons}
          title="Weapons"
          onChoose={(weapon) => toggle(weapons, weapon, setWeapons)}
        />
        <OptionList
          options={places}
          title="Places"
          onChoose={(place) => toggle(places, place, setPlaces)}
        />
      </Layout>
      {picker && <PickerDialog {...picker} />}
    </div>
  )
}
