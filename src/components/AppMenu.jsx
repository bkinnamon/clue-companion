import React from 'react'
import classes from './AppMenu.css'

export function AppMenu({
  actions = [],
  isOpen = false,
  title = 'Menu',
  onClose = () => {},
}) {
  const menuClass = classes.menu + (isOpen ? ' ' + classes.open : '')

  function handleChoose(action) {
    onClose()
    action.do()
  }

  return (
    <div className={menuClass}>
      <h2 className={classes.title}>{title}</h2>
      <div className={classes.actions}>
        {actions.map((action) => (
          <button
            key={action.text}
            className={classes.action}
            onClick={() => handleChoose(action)}
          >
            {action.text}
          </button>
        ))}
      </div>
    </div>
  )
}
