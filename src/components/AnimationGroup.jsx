import React, { useEffect, useRef, useState } from 'react'

export const AnimationGroup = ({ children }) => {
  const [list, setList] = useState(children)

  const listRefs = list.map(() => useRef())

  useEffect(() => {
    if (children.length !== list.length) {
      setList(children)
      return
    }
    children.forEach((child, index) => {
      if (
        child.key === list[index].key &&
        child.props.className.includes('eliminated') ===
          list[index].props.className.includes('eliminated')
      ) {
        return
      }
      const oldChildIndex = list.findIndex((item) => item.key === child.key)
      const oldTop = listRefs[oldChildIndex].current.offsetTop
      const newTop = listRefs[index].current.offsetTop
      listRefs[oldChildIndex].current.style.transition = 'transform 200ms'
      window.requestAnimationFrame(() => {
        listRefs[oldChildIndex].current.style.transform = `translate(0, ${
          newTop - oldTop
        }px)`
      })
      listRefs[oldChildIndex].current.addEventListener(
        'transitionend',
        (event) => {
          setList(children)
          const oldTransition = event.target.style.transition
          event.target.style.transition = 'transform 0s'
          window.requestAnimationFrame(() => {
            event.target.style.transform = ''
          })
        },
        { once: true }
      )
    })
  }, [children])

  return (
    <>
      {list.map((item, index) => {
        const TagName = item.type
        return <TagName {...item.props} key={item.key} ref={listRefs[index]} />
      })}
    </>
  )
}
