import React, { useState } from 'react'
import classes from './PickerDialog.css'
import { AnimationElement } from './AnimateElement'

export function PickerDialog({ options, title, onPick }) {
  const [isOpen, setIsOpen] = useState(true)
  const [value, setValue] = useState(null)

  function handleWrapperClick(event) {
    if (event.target === event.currentTarget) {
      setValue(null)
      setIsOpen(false)
    }
  }

  function handlePick(option) {
    setValue(option)
    setIsOpen(false)
  }

  function handleLeave() {
    onPick(value)
  }

  return (
    <div className={classes.wrapper} onClick={handleWrapperClick}>
      <AnimationElement
        baseClass={classes.picker}
        enterClass={classes.pickerEnter}
        leaveClass={classes.pickerLeave}
        isOpen={isOpen}
        onLeaveEnd={() => handleLeave()}
      >
        <div>
          <h2 className={classes.title}>{title}</h2>
          <div className={classes.options}>
            {options.map((option) => (
              <button
                key={option.value}
                className={classes.option}
                onClick={() => handlePick(option)}
              >
                {option.text}
              </button>
            ))}
          </div>
        </div>
      </AnimationElement>
    </div>
  )
}
