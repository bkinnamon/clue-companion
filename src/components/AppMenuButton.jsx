import React, { useState, useEffect } from 'react'
import classes from './AppMenuButton.css'

export function AppMenuButton({ isOpen, onClick }) {
  const buttonClass = classes.button + (isOpen ? ' ' + classes.open : '')

  return (
    <button className={buttonClass} onClick={onClick}>
      <hr className={classes.line} />
      <hr className={classes.line} />
      <hr className={classes.line} />
    </button>
  )
}
