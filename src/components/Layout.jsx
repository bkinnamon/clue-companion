import React, { useState } from 'react'
import { AppMenuButton } from './AppMenuButton'
import classes from './Layout.css'
import { AppMenu } from './AppMenu'

export function Layout({ actions, children, title }) {
  const [isOpen, setIsOpen] = useState(false)

  function toggleMenu() {
    setIsOpen(!isOpen)
  }

  return (
    <>
      <header className={classes.header}>
        <h1 className={classes.titleApp}>Clue Companion</h1>
        <AppMenuButton isOpen={isOpen} onClick={toggleMenu} />
      </header>
      <main className={classes.main}>
        <h2 className={classes.titlePage}>{title}</h2>
        {children}
      </main>
      <AppMenu
        actions={actions}
        isOpen={isOpen}
        title="Actions"
        onClose={() => setIsOpen(false)}
      />
    </>
  )
}
