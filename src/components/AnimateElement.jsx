import React, { useState, useEffect } from 'react'

export function AnimationElement({
  baseClass,
  children,
  enterClass,
  leaveClass,
  isOpen,
  onEnterEnd = () => {},
  onLeaveEnd = () => {},
}) {
  const [isEntering, setIsEntering] = useState(false)
  const [isLeaving, setIsLeaving] = useState(false)
  const [isShowing, setIsShowing] = useState(false)
  const [classes, setClasses] = useState(baseClass.split(' '))

  useEffect(() => {
    if (isOpen) {
      handleEnter()
    } else {
      handleLeave()
    }
  }, [isOpen])

  function handleEnter() {
    setClasses([...baseClass.split(' '), enterClass])
    setIsEntering(true)
    setIsLeaving(false)
    setIsShowing(true)
  }

  function handleLeave() {
    setClasses([...baseClass.split(' '), leaveClass])
    setIsEntering(false)
    setIsLeaving(true)
  }

  function handleAnimationEnd() {
    if (isEntering) {
      setIsEntering(false)
      setClasses(baseClass.split(' '))
      onEnterEnd()
    }
    if (isLeaving) {
      setIsLeaving(false)
      setIsShowing(false)
      setClasses(baseClass.split(' '))
      onLeaveEnd()
    }
  }

  const Child = children.type

  return (
    <>
      {isShowing && (
        <Child
          {...children.props}
          className={classes.join(' ')}
          onAnimationEnd={() => handleAnimationEnd()}
        />
      )}
    </>
  )
}
