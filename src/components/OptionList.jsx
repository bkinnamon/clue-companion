import React, { useState } from 'react'
import { AnimationGroup } from './AnimationGroup'
import classes from './OptionList.css'
import { AnimationElement } from './AnimateElement'

export function OptionList({ options, title, onChoose }) {
  const [isOpen, setIsOpen] = useState(true)

  // const available = options.filter((o) => !o.eliminated)
  // const eliminated = options.filter((o) => o.eliminated)

  options.sort((a, b) => {
    if (a.eliminated && !b.eliminated) return 1
    if (!a.eliminated && b.eliminated) return -1
    if (a.name < b.name) return -1
    if (a.name > b.name) return 1
    return 0
  })

  return (
    <div className={classes.wrapper}>
      <button className={classes.toggle} onClick={() => setIsOpen(!isOpen)}>
        <h3 className={classes.title}>{title}</h3>
      </button>
      <AnimationElement
        baseClass={classes.options}
        enterClass={classes.optionsEnter}
        leaveClass={classes.optionsLeave}
        isOpen={isOpen}
      >
        <div>
          <AnimationGroup>
            {options.map((option) => {
              const buttonClass =
                classes.option +
                (option.eliminated ? ' ' + classes.eliminated : '')
              return (
                <button
                  key={option.name}
                  className={buttonClass}
                  onClick={() => onChoose(option)}
                >
                  {option.name}
                </button>
              )
            })}
          </AnimationGroup>
        </div>
      </AnimationElement>
    </div>
  )
}
