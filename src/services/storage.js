export function saveToStorage(key, obj) {
  window.localStorage.setItem(key, JSON.stringify(obj))
}

export function getFromStorage(key) {
  return JSON.parse(window.localStorage.getItem(key))
}
